/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.test_bed;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import rvme.RegioVincoMapEditor;
import rvme.data.DataManager;
import rvme.file.FileManager;

/**
 *
 * @author vz97
 */
class Test extends Stage{
    
    //Pane polygonPane;
    Scene polygonScene;
    ArrayList<Polygon>[] polygonArray;
    String[][] nameArray;
    File[][] fileArray;
    Color[] colorArray;
    RegioVincoMapEditor app;
    //Group g;
    
    public Test(){
        //super();
        //polygonPane = new Pane();
        //Path path = Paths.get("");
        //System.out.println("@@" + path.toAbsolutePath().toString());
        polygonArray = new ArrayList[1];
        //g = new Group();
        //g.setAutoSizeChildren(true);
        try{
            //FileChooser fc = new FileChooser();
            //File selectedFile = fc.showOpenDialog(this);
            //now i have an array of arraylist with polygons with coordinates.
            //loadObjects("/Users/vz97/Downloads/HW5SampleData/raw_map_data/Andorra.json");
            //next?
            //make the following into methods:
            //lets name the 7 places, hardcode style (polygonArray[0] = first
            //state, polygonArray[1] = second state, etc., into an array (hashtable?)
            initializeNameArray();
            //then, lets incorporate the pictures with hardcode, taking the
            //file names and putting them into a file 2d array(leader, flag).
            initializeFileArray();
            //color array now.
            initializeColorArray();
            //export into custom JSON file
            //finally fuk this homework jesus.
            FileChooser fc = new FileChooser();
            File selectedFile = fc.showSaveDialog(this);
            //exportIntoJSON(selectedFile);
            FileManager f = new FileManager();
            JsonObject jo = f.exportIntoJSON(nameArray, fileArray, colorArray,
                    "Andorra", 1, "BLACK", "/Users/vz97/Downloads/RegioVincoMapEditor/files/raw_map_data/Slovakia.json", "omglol", 
                    "/files/export/The World/Europe/Andorra Flag.png",
                    1, 500, 500, "/files/export/The World/Europe/Andorra/Andorra.png");
            f.saveData(new DataManager(app), selectedFile.toString(), jo);
            System.exit(2);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void init(){
        //polygonScene = new Scene(g, 802, 536);
        //this.setScene(polygonScene);
        //this.show();
    }
    //Map Saving - create a package called test_bed and inside put a
    //driver test class (a class with a main method) called TestSave that 
    //will serve to demonstrate that map saving works.
    //In order to test saving, 
    //let's use Andorra. Have your driver class hard code the creation of 
    //all the necessary data values such that the Andorra map and data could 
    //be created. This means everything. The map data, the images, 
    //the background color, the border color and thickness, etc. Note that your
    //TestSave class would artificially initialize all the data such that it can
    //then be used by your file manager to save it to /work/Andorra.json,
    //which again, would be in your own JSON format.
    
     // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    private void loadObjects(String filePath) throws IOException{
        //use loadjsonfile method
        // CLEAR THE OLD DATA OUT
	//DataManager dataManager = (DataManager)data;
	//dataManager.reset();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
                
        //load number of subregions
        //subregions array:
        //number of polygons in this region
        Polygon poly;
        JsonArray subregionArray = json.getJsonArray("SUBREGIONS");
        JsonArray subregionPolygonArray;
        JsonArray subregionPolygonArrayObjectArray;
        JsonObject subregionPolygonObject;
        JsonObject xAndYObject;
        
        try{
            polygonArray = new ArrayList[subregionArray.size()];
            for(int i=0; i<polygonArray.length; i++){
                polygonArray[i] = new ArrayList();
            }
            //the number of subregions
            for(int i=0; i<subregionArray.size(); i++){
                subregionPolygonObject = subregionArray.getJsonObject(i);
                subregionPolygonArray = subregionPolygonObject.getJsonArray("SUBREGION_POLYGONS");
                //the number of subregion polygons
                for(int j=0; j<subregionPolygonArray.size(); j++){
                    //put in values of array into the arraylist of arrays
                    //create jsonobject?
                    subregionPolygonArrayObjectArray = subregionPolygonArray.getJsonArray(j);
                    poly = new Polygon();
                    poly.setStroke(Color.BLACK);
                    poly.setStrokeWidth(1);
                    //this iterates over the points in per subregion polygon
                    for(int k=0; k<subregionPolygonArrayObjectArray.size(); k++){
                        xAndYObject = subregionPolygonArrayObjectArray.getJsonObject(k);
                        //add the points to the polygon to evenutally return
                        addPoints(poly, xAndYObject);
                    }
                    polygonArray[i].add(poly);
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        /*
        for(int i=0; i<polygonArray.length; i++){
            for(int j=0; j<polygonArray[i].size(); j++){
                polygonPane.getChildren().add(polygonArray[i].get(j));
            }
        }
        */
        //polygonPane.sets
        //g = new Group();
        //g.setAutoSizeChildren(true);
        //g.getChildren().add(polygonPane);
    }
    
    public void addPoints(Polygon poly, JsonObject object){
        poly.getPoints().addAll(new Double[]{
            (getDataAsDouble(object, "X")+180)/360*802,
            (180-(getDataAsDouble(object, "Y")+90))/180*536
        });
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    private void initializeNameArray(){
        //hardcode bois
        nameArray = new String[7][3];
        for(int i=0; i<nameArray.length; i++){
            for(int j=0; j<nameArray[i].length; j++){
                nameArray[i][j] = "";
            }
        }
        nameArray[0][0] = "Ordino";
        nameArray[0][1] = "Ordino (town)";
        nameArray[0][2] = "Ventura Espot";
        
        nameArray[1][0] = "Canillo";
        nameArray[1][1] = "Canillo (town)";
        nameArray[1][2] = "Enric Casadevall Medrano";
        
        nameArray[2][0] = "Encamp";
        nameArray[2][1] = "Encampe (town)";
        nameArray[2][2] = "Miquel Alís Font";
        
        nameArray[3][0] = "Escaldes-Engordany";
        nameArray[3][1] = "Escaldes-Engordany (town)";
        nameArray[3][2] = "Montserrat Capdevila Pallarés";
        
        nameArray[4][0] = "La Massana";
        nameArray[4][1] = "La Massana (town)";
        nameArray[4][2] = "Josep Areny";
        
        nameArray[5][0] = "Andorra la Vella";
        nameArray[5][1] = "Andorra la Vella (city)";
        nameArray[5][2] = "Maria Rosa Ferrer Obiols";
        
        nameArray[6][0] = "Sant Julia de Loria";
        nameArray[6][1] = "Sant Julia de Loria (town)";
        nameArray[6][2] = "Josep Pintat Forné";
    }
    
    private void initializeFileArray(){
        //here we hard code the file array values
        fileArray = new File[7][2];
        fileArray[0][0] = new File("/files/export/The World/Europe/Andorra/Ordino Flag.png");
        fileArray[0][1] = new File("/files/export/The World/Europe/Andorra/Ventura Espot.png");
        
        fileArray[1][0] = new File("/files/export/The World/Europe/Andorra/Carillo Flag.png");
        fileArray[1][1] = new File("/files/export/The World/Europe/Andorra/Enric Casadevall Medrano.png");
        
        fileArray[2][0] = new File("/files/export/The World/Europe/Andorra/Encamp Flag.png");
        fileArray[2][1] = new File("/files/export/The World/Europe/Andorra/Miquel Al°s Font.png");
        
        fileArray[3][0] = new File("/files/export/The World/Europe/Andorra/Escaldes-Engordany Flag.png");
        fileArray[3][1] = new File("/files/export/The World/Europe/Andorra/Montserrat Capdevila PallarÇs.png");
        
        fileArray[4][0] = new File("/files/export/The World/Europe/Andorra/La Massana Flag.png");
        fileArray[4][1] = new File("/files/export/The World/Europe/Andorra/Josep Areny.png");
        
        fileArray[5][0] = new File("/files/export/The World/Europe/Andorra/Andorra la Vella Flag.png");
        fileArray[5][1] = new File("/files/export/The World/Europe/Andorra/Maria Rosa Ferrer Obiols.png");
        
        fileArray[6][0] = new File("/files/export/The World/Europe/Andorra/Sant Julia de Loria Flag.png");
        fileArray[6][1] = new File("/files/export/The World/Europe/Andorra/Josep Pintat FornÇ.png");
    }
    
    private void initializeColorArray(){
        colorArray = new Color[7];
        colorArray[0] = Color.rgb(200,200,200);
        colorArray[1] = Color.rgb(198,198,198);
        colorArray[2] = Color.rgb(196,196,196);
        colorArray[3] = Color.rgb(194,194,194);
        colorArray[4] = Color.rgb(192,192,192);
        colorArray[5] = Color.rgb(190,190,190);
        colorArray[6] = Color.rgb(188,188,188);
    }
}

public class TestSave extends Application{
    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Test t = new Test();
        //t.init();
    }
}
