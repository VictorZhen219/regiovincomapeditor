/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.gui;

import java.awt.MouseInfo;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Background;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import rvme.RegioVincoMapEditor;
import rvme.audio.AudioManager;
import rvme.data.DataManager;
import rvme.data.Subregion;
import rvme.file.FileManager;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppPropertyType.*;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import saf.ui.DimensionDialogSingleton;
import saf.ui.EditMapDialogSingleton;
import saf.ui.NewMapDialogSingleton;
import saf.ui.RenameMapDialogSingleton;

/**
 *
 * @author vz97
 */
public class Workspace extends AppWorkspaceComponent{
    RegioVincoMapEditor app;
    SplitPane splitPane;
    Pane mapPane;
    TableView<Subregion> tableView;
    TableColumn nameColumn;
    TableColumn capitalColumn;
    TableColumn leaderColumn;
    AudioManager audio;
    protected Button renameMapButton;
    protected Button addImageButton;
    protected Button removeButton;
    protected VBox backgroundColorChooserBox;
    protected VBox borderColorChooserBox;
    protected Label backgroundColorChooserLabel;
    protected Label borderColorChooserLabel;
    protected ColorPicker backgroundColorChooser;
    protected ColorPicker borderColorChooser;
    protected VBox borderBox;
    protected VBox zoomBox;
    protected Label borderLabel;
    protected Label zoomLabel;
    protected Slider borderThicknessSlider;
    protected Slider zoomSlider;
    protected Button audioButton;
    protected Button grayScaleButton;
    protected Button mapDimensionButton;
    protected VBox imageBox;
    protected VBox tableBox;
    private FlowPane fileToolbarPane;
    private int zoomedOut = 0;
    private ArrayList<Double> previousZooms;
    private int translateScale = 50;
    private String currentName;
    private String currentDirectory;
    private String parentDirectory;
    private String currentFile;
    boolean playingMusic = false;
    StackPane stackPane;
    Pane imagePane;
    int currentIndex = 0;
    ArrayList<ImageView> imageList;
    Group g = new Group();
    Polygon currentBackground = null;
    double currentBorderThickness;
    double currentZoom;
    double groupX;
    double groupY;
    Color currentBackgroundColor;
    Color currentBorderColor;
    
    public Workspace(RegioVincoMapEditor initApp){
        app = initApp;
        workspace = new Pane();
        splitPane = new SplitPane();
        previousZooms = new ArrayList();
        imageList = new ArrayList();
        mapPane = new Pane();
        mapPane.setMaxSize(app.getGUI().getWindow().getScene().getWidth()/2, 
                app.getGUI().getWindow().getScene().getHeight()/2);
        mapPane.setMinSize(app.getGUI().getWindow().getScene().getWidth()/2, 
                app.getGUI().getWindow().getScene().getHeight()/2);
        initializeEditToolbarAndMore();
        //splitPane.setMinSize(workspace.getMaxWidth(), workspace.getMaxHeight());
        initializeSplitPane();
        initializeListeners();
    }
    
    public Pane getMapPane(){
        return mapPane;
    }
    
    public Group getGroup(){
        return g;
    }
    
    private void initializeEditToolbarAndMore(){
        fileToolbarPane = (FlowPane) app.getGUI().getAppPane().getTop();

        //now for our custom edit toolbar
        Line divider = new Line(0, 0, 0, fileToolbarPane.getHeight());
        divider.setStroke(Color.web("#66ccff"));
        divider.setStrokeWidth(5);
        fileToolbarPane.getChildren().add(divider);
        fileToolbarPane.getChildren().add(new Label("Edit:"));

        renameMapButton = app.getGUI().initChildButton(fileToolbarPane, RENAME_MAP_ICON.toString(), RENAME_MAP_TOOLTIP.toString(), false);
        addImageButton = app.getGUI().initChildButton(fileToolbarPane, ADD_IMAGE_ICON.toString(), ADD_IMAGE_TOOLTIP.toString(), false);
        removeButton = app.getGUI().initChildButton(fileToolbarPane, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), false);
        //colorpickers now
        
        backgroundColorChooserBox = new VBox();
        backgroundColorChooserLabel = new Label("Background Color:");
        backgroundColorChooser = new ColorPicker();
        backgroundColorChooserBox.getChildren().addAll(backgroundColorChooserLabel, backgroundColorChooser);
        
        borderColorChooserBox = new VBox();
        borderColorChooserLabel = new Label("Border Color:");
        borderColorChooser = new ColorPicker();
        borderColorChooserBox.getChildren().addAll(borderColorChooserLabel, borderColorChooser);
        
        fileToolbarPane.getChildren().addAll(backgroundColorChooserBox, borderColorChooserBox);
        //sliders now
        borderLabel = new Label("Border Thickness: ");
        borderThicknessSlider = new Slider(.005,1,.005);
        borderBox = new VBox();
        borderBox.getChildren().addAll(borderLabel, borderThicknessSlider);
        zoomLabel = new Label("Zoom: ");
        zoomSlider = new Slider(1,200,1);
        zoomBox = new VBox();
        zoomBox.getChildren().addAll(zoomLabel, zoomSlider);
        fileToolbarPane.getChildren().addAll(borderBox, zoomBox);
        //last button, audio
        audioButton = app.getGUI().initChildButton(fileToolbarPane, AUDIO_ICON.toString(), AUDIO_TOOLTIP.toString(), false);
        grayScaleButton = app.getGUI().initChildButton(fileToolbarPane, GRAY_SCALE_ICON.toString(), GRAY_SCALE_TOOLTIP.toString(), false);
        mapDimensionButton = app.getGUI().initChildButton(fileToolbarPane, DIMENSION_ICON.toString(), DIMENSION_TOOLTIP.toString(), false);
    }
    
    public void initializeSplitPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //add an image view to the left, a table view to the right
        
        //mapPane.setPrefSize(800, 800);
        imageBox = new VBox();
        tableBox = new VBox();
        //imageView.set
        //splitPane.getItems().add(mapPane);
         
        //right side now!
        tableView = new TableView();
        nameColumn = new TableColumn(props.getProperty(PropertyType.NAME));
        capitalColumn = new TableColumn(props.getProperty(PropertyType.CAPITAL));
        leaderColumn = new TableColumn(props.getProperty(PropertyType.LEADER));
        //i dont know what this means and im too afraid to ask
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        capitalColumn.setCellValueFactory(new PropertyValueFactory<String, String>("capital"));
        leaderColumn.setCellValueFactory(new PropertyValueFactory<String, String>("leader"));
        //add to the tableview
        tableView.getColumns().add(nameColumn);
        tableView.getColumns().add(capitalColumn);
        tableView.getColumns().add(leaderColumn);
        
        DataManager data = (DataManager) app.getDataComponent();
        tableView.setItems(observableArrayList(data.getItems()));
        tableBox.getChildren().add(new Label("Information Table"));
        tableBox.getChildren().add(tableView);
        
        g = new Group();
        stackPane = new StackPane();
        //imagePane = new Pane();
        mapPane.getChildren().add(g);
        //stackPane.getChildren().addAll(mapPane, imagePane);
        stackPane.getChildren().add(mapPane);
        
        //finally add to the splitpane
        //mapPane.set
        splitPane.getItems().addAll(stackPane, tableBox);
        workspace.getChildren().add(splitPane);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        splitPane.setDividerPositions(.5f);
        //SplitPane.setResizableWithParent(workspace, true);
        //SplitPane.se
        
        //for sizing
        //"if it aint broke dont fix it"
        workspace.setMinWidth(app.getGUI().getWindow().getScene().getWidth());
        workspace.setMinHeight(app.getGUI().getWindow().getScene().getHeight());
        workspace.setMaxWidth(app.getGUI().getWindow().getScene().getWidth());
        workspace.setMaxHeight(app.getGUI().getWindow().getScene().getHeight());
        splitPane.setMinWidth(app.getGUI().getWindow().getScene().getWidth());
        splitPane.setMinHeight(app.getGUI().getWindow().getScene().getHeight());
        splitPane.setMaxWidth(app.getGUI().getWindow().getScene().getWidth());
        splitPane.setMaxHeight(app.getGUI().getWindow().getScene().getHeight());
        
        //workspace.setStyle("-fx-background-color: #00e6b8");
    }
    
    private void initializeListeners(){
        
        NewMapDialogSingleton singleton = NewMapDialogSingleton.getSingleton();
        singleton.getOKButton().setOnAction(e ->{
            //we get the information from the singleton
            String name = singleton.getName();
            String folderDirectory = singleton.getParentDirectory();
            String mapDirectory = singleton.getMapDirectory();
            
            setCurrentName(name);
            new File(folderDirectory + "/" + name).mkdir();
            setCurrentDirectory(folderDirectory + "/" + name);
            setParentDirectory(folderDirectory);
            try {
                new File(folderDirectory + "/" + name + "/" + name).createNewFile();
                setCurrentFile(folderDirectory + "/" + name + "/" + name);
            } catch (IOException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            FileManager fManager = (FileManager) app.getFileComponent();
            DataManager dManager = (DataManager) app.getDataComponent();
            try{
                fManager.loadPane(dManager, mapDirectory);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            
            singleton.hide();
        });
        singleton.getCancelButton().setOnAction(e -> {
            singleton.hide();
        });
        
        renameMapButton.setOnAction(e -> {
            RenameMapDialogSingleton rmSingleton = RenameMapDialogSingleton.getSingleton();
            rmSingleton.init(app.getGUI().getWindow());
            rmSingleton.show("Rename Map Dialog", "Rename the Map:");
            String newName = rmSingleton.getMapText();
            File d = new File(getCurrentDirectory());
            File d1 = new File(getParentDirectory() + "/" + newName);
            File d2 = new File(getCurrentFile());
            File d3 = new File(getCurrentDirectory() + "/" + newName);
            boolean f = d2.renameTo(d3);
            d.renameTo(d1);
            setCurrentDirectory(d1.toString());
            setCurrentFile(d3.toString());
        });
        
        audioButton.setOnAction(e -> {
            playingMusic = !playingMusic;
            if(playingMusic){
                //TODO:
            }
            else{
                
            }
        });
        
        addImageButton.setOnAction(e ->{
            FileChooser f = new FileChooser();
            File toAdd = f.showOpenDialog(app.getGUI().getWindow());
            if(toAdd.isFile()){
                Image i = new Image("file:" + toAdd.toString());
                //i.
                ImageView imageView = new ImageView(i);
                imageList.add(imageView);
                
                imageView.setOnMouseClicked(event -> {
                    currentIndex = imageList.indexOf(imageView);
                });
                imageView.setOnMouseDragged(event -> {
                    imageView.setX(event.getX());
                    imageView.setY(event.getY());
                });
                mapPane.getChildren().add(imageView);
            }
        });
        removeButton.setOnAction(e ->{
            ImageView toRemove = imageList.get(currentIndex);
            imageList.remove(currentIndex);
            mapPane.getChildren().remove(toRemove);
        });
        
        backgroundColorChooser.setOnAction( e ->{
            //change back to stackpane
            Color c = backgroundColorChooser.getValue();
            if(currentBackground!=null){
                stackPane.getChildren().remove(currentBackground);
                currentBackground = null;
            }
            Polygon p = new Polygon();
            p.getPoints().addAll(new Double[]{0.0, 0.0,
            0.0, stackPane.getHeight(),
            stackPane.getWidth(), stackPane.getHeight(),
            stackPane.getWidth(), 0.0});
            p.setFill(c);
            stackPane.getChildren().add(p);
            p.toBack();
            currentBackground = p;
        });
        
        borderColorChooser.setOnAction( e-> {
            Color c = borderColorChooser.getValue();
            DataManager m = (DataManager) app.getDataComponent();
                ArrayList<Polygon>[] polygonList = m.getPolygonList();
                if(polygonList.length<257){
                    int constant = 256/polygonList.length;
                    for(int i=polygonList.length-1; i>=0; i--){
                        for(int j=0; j<polygonList[i].size(); j++){
                            //c.
                            polygonList[i].get(j).setStroke(c);
                        }
                    }
                }
        });
        
        borderThicknessSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                DataManager m = (DataManager) app.getDataComponent();
                ArrayList<Polygon>[] polygonList = m.getPolygonList();
                if(polygonList.length<257){
                    int constant = 256/polygonList.length;
                    for(int i=polygonList.length-1; i>=0; i--){
                        for(int j=0; j<polygonList[i].size(); j++){
                            //c.
                            polygonList[i].get(j).setStrokeWidth(new_val.doubleValue());
                        }
                    }
                }
            }
        });
        
        FileManager f = (FileManager) app.getFileComponent();
        Workspace w = (Workspace) app.getWorkspaceComponent();
        //Group g = f.getPolygonGroup();
        
        
        zoomSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number o, Number n) {
                    g.setScaleX(n.doubleValue());
                    g.setScaleY(n.doubleValue());
            }
        });
        
        //
        //protected Button grayScaleButton;
    //protected Button mapDimensionButton;
        grayScaleButton.setOnAction( e->{
            DataManager m = (DataManager) app.getDataComponent();
            ArrayList<Polygon>[] polygonList = m.getPolygonList();
            if(polygonList.length<257){
                int constant = 256/polygonList.length;
                for(int i=polygonList.length-1; i>=0; i--){
                    for(int j=0; j<polygonList[i].size(); j++){
                        //c.
                        polygonList[i].get(j).setFill(Color.rgb(i*constant,i*constant,i*constant));
                    }
                }
            }
        });
        mapDimensionButton.setOnAction( e->{
            DimensionDialogSingleton singletonn = DimensionDialogSingleton.getSingleton();
            singletonn.init(app.getGUI().getWindow());
            singletonn.show("Dimension Dialog", "Enter new map dimensions.");
            int x = Integer.parseInt(singletonn.getXText());
            int y = Integer.parseInt(singletonn.getYText());
            mapPane.setPrefSize(x, y);
        });
        
        EditMapDialogSingleton editSingleton = EditMapDialogSingleton.getSingleton();
        try {
            editSingleton.init();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        tableView.setOnMouseClicked(e -> {
            //itemsTable.getSelectionModel().
            //select the current todoitem
            if (e.getClickCount() == 2) {
                editSubregion(editSingleton);
            }
        });
        app.getGUI().getPrimaryScene().setOnKeyPressed(e -> {
            mapPane.requestFocus();
            switch(e.getCode()){
                case UP:
                    g.setTranslateY(g.getTranslateY()+translateScale);
                    break;
                case DOWN:
                    g.setTranslateY(g.getTranslateY()-translateScale);
                    break;
                case LEFT:
                    g.setTranslateX(g.getTranslateX()+translateScale);
                    break;
                case RIGHT:
                    g.setTranslateX(g.getTranslateX()-translateScale);
                    break;
                default:
                    break;
            }
            e.consume();
        });
        
        DataManager m = (DataManager) app.getDataComponent();
        editSingleton.getOkButton().setOnAction(e->{
            Subregion sr = tableView.getSelectionModel().getSelectedItem();
            sr.setName(editSingleton.getNameField().getText());
            sr.setCapital(editSingleton.getCapitalField().getText());
            sr.setLeader(editSingleton.getLeaderField().getText());
            sr.setFlagPFile(editSingleton.getFlagField().getText());
            sr.setLeaderPFile(editSingleton.getLeaderPhotoField().getText());
            editSingleton.hide();
            tableView.refresh();
        });
        editSingleton.getNextButton().setOnAction(e->{
            int pos = tableView.getSelectionModel().getSelectedIndex();
            if(pos<m.getItems().size()-1){
                tableView.getSelectionModel().selectNext();
            }
            editSingleton.setNameField(tableView.getSelectionModel().getSelectedItem().getName());
                    editSingleton.setCapitalField(tableView.getSelectionModel().getSelectedItem().getCapital());
                    editSingleton.setLeaderField(tableView.getSelectionModel().getSelectedItem().getLeader());
                    editSingleton.setFlagField(tableView.getSelectionModel().getSelectedItem().getFlagPFile());
                    editSingleton.setLeaderPhotoField(tableView.getSelectionModel().getSelectedItem().getLeaderPFile());
        });
        editSingleton.getPrevButton().setOnAction(e->{
            int pos = tableView.getSelectionModel().getSelectedIndex();
            if(pos>0){
                tableView.getSelectionModel().selectPrevious();
            }
            editSingleton.setNameField(tableView.getSelectionModel().getSelectedItem().getName());
                    editSingleton.setCapitalField(tableView.getSelectionModel().getSelectedItem().getCapital());
                    editSingleton.setLeaderField(tableView.getSelectionModel().getSelectedItem().getLeader());
                    editSingleton.setFlagField(tableView.getSelectionModel().getSelectedItem().getFlagPFile());
                    editSingleton.setLeaderPhotoField(tableView.getSelectionModel().getSelectedItem().getLeaderPFile());
        });
        
    }
    
    @Override
    public void reloadWorkspace() {
        DataManager dataManager = (DataManager)app.getDataComponent();
        tableView.setItems(observableArrayList(dataManager.getItems()));
    }

    @Override
    public void initStyle() {
        //do css here
        
    }
    
    public void highlightRow(int pos){
        tableView.getSelectionModel().select(pos);
    }

    /**
     * @return the currentName
     */
    public String getCurrentName() {
        return currentName;
    }

    /**
     * @param currentName the currentName to set
     */
    public void setCurrentName(String currentName) {
        this.currentName = currentName;
    }

    /**
     * @return the currentDirectory
     */
    public String getCurrentDirectory() {
        return currentDirectory;
    }

    /**
     * @param currentDirectory the currentDirectory to set
     */
    public void setCurrentDirectory(String currentDirectory) {
        this.currentDirectory = currentDirectory;
    }

    /**
     * @return the parentDirectory
     */
    public String getParentDirectory() {
        return parentDirectory;
    }

    /**
     * @param parentDirectory the parentDirectory to set
     */
    public void setParentDirectory(String parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    /**
     * @return the currentFile
     */
    public String getCurrentFile() {
        return currentFile;
    }

    /**
     * @param currentFile the currentFile to set
     */
    public void setCurrentFile(String currentFile) {
        this.currentFile = currentFile;
    }
    
    public void editSubregion(EditMapDialogSingleton editSingleton){
        if(tableView.getSelectionModel().getSelectedItem()!=null){
                    editSingleton.setNameField(tableView.getSelectionModel().getSelectedItem().getName());
                    editSingleton.setCapitalField(tableView.getSelectionModel().getSelectedItem().getCapital());
                    editSingleton.setLeaderField(tableView.getSelectionModel().getSelectedItem().getLeader());
                    editSingleton.setFlagField(tableView.getSelectionModel().getSelectedItem().getFlagPFile());
                    editSingleton.setLeaderPhotoField(tableView.getSelectionModel().getSelectedItem().getLeaderPFile());
                    editSingleton.show("Edit");
        }
    }
}