/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.drivers;

import java.net.URL;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.PropertyType;

/**
 *
 * @author vz97
 */
class Tester extends Stage{
    Label nameLabel;
    Label capitalLabel;
    Label leaderLabel;
    TextField nameField;
    TextField capitalField;
    TextField leaderField;
    TextField flagField;
    TextField leaderPhotoField;
    Label flagLabel;
    Label leaderPhotoLabel;
    Button nextButton;
    Button prevButton;
    Button okButton;
    Button flagButton;
    Button leaderPhotoButton;

    public void init() throws Exception {
        GridPane grid = new GridPane();
        //initialize stuff
        nameLabel = new Label("Name: ");
        capitalLabel = new Label("Capital: ");
        leaderLabel = new Label("Leader: ");
        flagLabel = new Label("Current Flag: ");
        leaderPhotoLabel = new Label("Current Photo of Leader");
        nameField = new TextField("Alaska");
        capitalField = new TextField("???");
        leaderField = new TextField("???");
        flagField = new TextField("Users/flag.png");
        leaderPhotoField = new TextField("Users/leader.png");
        nextButton = new Button("Next");
        prevButton = new Button("Previous");
        okButton = new Button("Ok");
        flagButton = new Button("Choose Flag Photo");
        leaderPhotoButton = new Button("Choose Leader Photo");
        
        //add to grid
        grid.add(nameLabel, 0, 0);
        grid.add(capitalLabel, 0, 1);
        grid.add(leaderLabel, 0, 2);
        grid.add(nameField, 1, 0);
        grid.add(capitalField, 1, 1);
        grid.add(leaderField, 1, 2);
        grid.add(flagField, 1, 3);
        grid.add(leaderPhotoField, 1, 4);
        grid.add(flagButton, 2, 3);
        grid.add(leaderPhotoButton, 2, 4);
        grid.add(flagLabel, 0, 3);
        grid.add(leaderPhotoLabel, 0, 4);
        grid.add(prevButton, 0, 5);
        grid.add(okButton, 1, 5);
        grid.add(nextButton, 2, 5);
        
        //add grid to scene to stage
        PropertiesManager manager = PropertiesManager.getPropertiesManager();
        Scene s = new Scene(grid);
        //System.out.println(manager.getProperty(PropertyType.NAME));
        URL resource = this.getClass().getClassLoader().
                getResource("rvme/css/rvme_style.css");
        String resourceString = resource.toExternalForm();
        s.getStylesheets().add(resourceString);
        grid.getStyleClass().add("new_dialog");
        this.setScene(s);
        this.show();
    }
}

public class EditViewDialogTestDriver extends Application{
    
    public static void main(String[] args){
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage){
        try {
            Tester d = new Tester();
            d.init();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
