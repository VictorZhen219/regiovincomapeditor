/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.drivers;

import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.PropertyType;

/**
 *
 * @author vz97
 */
class TestDriver extends Stage{
    
    public static final String FILE_PROTOCOL = "file:";
    public static final String PATH_CSS = "./css/";
    TextField nameField;
    FileChooser parentDirectoryChooser;
    FileChooser mapChooser;
    Button parentDirectoryButton;
    Button mapButton;
    Button okButton;
    Button cancelButton;
    TextField parentDirectoryField;
    TextField mapField;
    File parentFile;
    File mapFile;

    public void init() throws Exception {
        GridPane gPane = new GridPane();
        gPane.setHgap(5);
        gPane.setVgap(5);
        gPane.add(new Label("Name: "), 0, 0);
        gPane.add(new Label("Parent Directory: "), 0, 1);
        gPane.add(new Label("Map File: "), 0, 2);
        
        nameField = new TextField("");
        parentDirectoryField = new TextField("Users/temp");
        mapField = new TextField("Users/map");
        gPane.add(nameField, 1,0);
        gPane.add(parentDirectoryField, 1, 1);
        gPane.add(mapField, 1, 2);
        parentDirectoryChooser = new FileChooser();
        mapChooser = new FileChooser();
        parentDirectoryButton = new Button("Choose Parent Directory");
        gPane.add(parentDirectoryButton, 2,1);
        mapButton = new Button("Choose Map");
        gPane.add(mapButton, 2, 2);
        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
        
        gPane.add(okButton, 0,3);
        gPane.add(cancelButton, 1,3);
        
        PropertiesManager manager = PropertiesManager.getPropertiesManager();
        Scene s = new Scene(gPane);
        //System.out.println(manager.getProperty(PropertyType.DIRECTORY));
        URL resource = this.getClass().getClassLoader().
                getResource("rvme/css/rvme_style.css");
        String resourceString = resource.toExternalForm();
        s.getStylesheets().add(resourceString);
        gPane.getStyleClass().add("edit_dialog");
        
        //parentFile = parentDirectoryChooser.getInitialDirectory();
        //parentDirectoryField.setText(parentFile.getPath());
        //mapFile = mapChooser.getInitialDirectory();
        //mapField.setText(mapFile.getPath());
        
        this.setScene(s);
        this.show();
        
        parentDirectoryButton.setOnAction(e -> {
            //parentFile = parentDirectoryChooser.showOpenDialog(primaryStage);
            //parentDirectoryField.setText(parentFile.toString());
        });
        mapButton.setOnAction(e -> {
            //mapFile = mapChooser.showOpenDialog(primaryStage);
            //mapField.setText(mapFile.toString());
        });
    }
}

public class NewMapDialogTestDriver extends Application{
    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        TestDriver d = new TestDriver();
        d.init();
    }
}