/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme;

/**
 *
 * @author vz97
 */
public enum PropertyType {
    OK_PROMPT,
    CANCEL_PROMPT,
    
    NAME,
    CAPITAL,
    LEADER,
    
    DIRECTORY,
    NEW_DIALOG,
    EDIT_DIALOG
}
