/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.data;

import java.io.File;
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import rvme.RegioVincoMapEditor;
import rvme.gui.Workspace;
import saf.components.AppDataComponent;
import saf.ui.EditMapDialogSingleton;

/**
 *
 * @author vz97
 */
public class DataManager implements AppDataComponent{

    RegioVincoMapEditor app;
    ArrayList<Polygon>[] polygonArrayList;
    ArrayList<Subregion> items;
    ArrayList<String> flagPhotos;
    ArrayList<String> leaderPhotos;
    ArrayList<Double>[] colors;
    File[][] fileArray;
    String name;
    
    public DataManager(RegioVincoMapEditor initApp){
        app = initApp;
        polygonArrayList = new ArrayList[0];
        items = new ArrayList<Subregion>();
        flagPhotos = new ArrayList<String>();
        leaderPhotos = new ArrayList<String>();
        fileArray = new File[0][0];
        colors = new ArrayList[3];
        for(int i=0; i<colors.length; i++){
            colors[i] = new ArrayList<Double>();
        }
        //temporary
        //addItem(new Subregion("Alaska City", "???", "???"));
        //addItem(new Subregion("idk any cities in alaksa", "???", "que"));
    }
    
    public ArrayList<Polygon>[] getPolygonList(){
        return polygonArrayList;
    }
    
    public ArrayList<Subregion> getItems(){
        return items;
    }
    
    public ArrayList<Double>[] getColors(){
        return colors;
    }
    
    public void addItem(Subregion item){
        items.add(item);
    }
    
    public void addFlag(String item){
        flagPhotos.add(item);
    }
    
    public void addLeader(String item){
        leaderPhotos.add(item);
    }
    
    public void addColor(Double item, int index){
        colors[index].add(item);
    }
    
    public void changeLengthOfArray(int newLength){
        polygonArrayList = new ArrayList[newLength];
        for(int i=0; i<newLength; i++){
            polygonArrayList[i] = new ArrayList();
        }
        fileArray = new File[newLength][2];
        for(int i=0; i<newLength; i++){
            for(int j=0; j<fileArray[i].length; j++){
                
            }
        }
    }
    
    /**
     * Adds a polygon to the array of arraylists
     * @param poly - the polygon to be added to the array of arraylists
     * @param position - the position of the array
     */
    public void addPolygon(Polygon poly, int position){
        poly.setFill(Color.GREEN);
        polygonArrayList[position].add(poly);
    }
    
    public void addListener(Polygon poly, int pos){
        poly.setOnMouseClicked(e -> {
            Workspace space = (Workspace) app.getWorkspaceComponent();
            space.highlightRow(pos);
            if (e.getClickCount() == 2) {
                EditMapDialogSingleton editSingleton = EditMapDialogSingleton.getSingleton();
                space.editSubregion(editSingleton);
            }
        });
    }
    
    /**
     * Show the polygons on the screen
     */
     public void displayData(){
        Workspace space = (Workspace) app.getWorkspaceComponent();
        for(int i=0; i<polygonArrayList.length; i++){
            for(int j=0; j<polygonArrayList[i].size(); j++){
                space.getGroup().getChildren().add(polygonArrayList[i].get(j));
            }
        }
    }
    
    /**
     * Returns the width and height of the scene
     */
    public double[] getWidthAndHeight(){
        /*
        return(new double[]{
            app.getGUI().getWindow().getScene().getWidth(),
            app.getGUI().getWindow().getScene().getHeight()-((FlowPane)app.getGUI().getAppPane().getTop()).getHeight()});
*/
        return (new double[]{640.0,343.0});
    }
    
    
    public void addPolygonsToGroup(Group g){
        for(int i=0; i<polygonArrayList.length; i++){
            for(int j=0; j<polygonArrayList[i].size(); j++){
                g.getChildren().add(polygonArrayList[i].get(j));
            }
        }
    }
    
    public void printAll(){
        //items, flagphotos, leaderphotos, color
        System.out.print("Flag Photo Directories: ");
        for(int i=0; i<flagPhotos.size(); i++){
            System.out.println(flagPhotos.get(i));
        }
        System.out.print("Leader Photo Directories: ");
        for(int i=0; i<leaderPhotos.size(); i++){
            System.out.println(leaderPhotos.get(i));
        }
        System.out.print("Color Values: ");
        for(int i=0; i<colors.length; i++){
            for(int j=0; j<colors[i].size(); j++){
                System.out.println(colors[i].get(j));
            }
        }
    }
    
    public void removeItem(Subregion item){
        items.remove(item);
    }
    
    @Override
    public void reset() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        space.reloadWorkspace();
    }
    
    public void setName(String s){
        name = s;
    }
    
    public String getName(){
        return name;
    }
    
}
