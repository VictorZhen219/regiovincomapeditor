/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.data;

/**
 *
 * @author vz97
 */
public class Subregion {
    
    private String name;
    private String capital;
    private String leader;
    private String flagPFile;
    private String leaderPFile;
    
    public Subregion(String initName, String initCapital, String initLeader){
        name = initName;
        capital = initCapital;
        leader = initLeader;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    /**
     * @return the flagPFile
     */
    public String getFlagPFile() {
        return flagPFile;
    }

    /**
     * @param flagPFile the flagPFile to set
     */
    public void setFlagPFile(String flagPFile) {
        this.flagPFile = flagPFile;
    }

    /**
     * @return the leaderPFile
     */
    public String getLeaderPFile() {
        return leaderPFile;
    }

    /**
     * @param leaderPFile the leaderPFile to set
     */
    public void setLeaderPFile(String leaderPFile) {
        this.leaderPFile = leaderPFile;
    }
    
}
