/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import rvme.data.DataManager;
import rvme.data.Subregion;
import rvme.gui.Workspace;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author vz97
 */
public class FileManager implements AppFileComponent{
    static final String JSON_X = "X";
    static final String JSON_Y = "Y";
    static final String SUBREGIONS = "SUBREGIONS";
    static final String SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
    Group polygonGroup = new Group();
    
    public Group getPolygonGroup(){
        return polygonGroup;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        System.out.println("asdfasdf");
        //get information from workspace
        /*
            JsonObject jo = f.exportIntoJSON(selectedFile, nameArray, fileArray, colorArray,
                    "Andorra", 1, "BLACK", selectedFile.toString(), "omglol", "/files/export/The World/Europe/Andorra Flag.png",
                    1, 500, 500, "/files/export/The World/Europe/Andorra/Andorra.png");
        */
        DataManager m = (DataManager) data;
        ArrayList<Subregion> al = m.getItems();
        String[][] nameArray = new String[al.size()][3];
        for(int i=0; i<al.size(); i++){
            nameArray[i][0] = al.get(i).getName();
            nameArray[i][1] = al.get(i).getCapital();
            nameArray[i][2] = al.get(i).getLeader();
        }
        
    }
    
    public void saveData(AppDataComponent data, String filePath, JsonObject toSave) throws IOException{
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(toSave);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(toSave);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException{
        VBox box = new VBox();
        Label progressLabel = new Label("0%");
        box.getChildren().add(progressLabel);
        ProgressBar progressBar = new ProgressBar();
        progressBar.setProgress(0);
        box.getChildren().add(progressBar);
        box.setAlignment(Pos.CENTER);
        box.setSpacing(5);
        Scene s = new Scene(box, 100, 50);
        Stage stage = new Stage();
        stage.setScene(s);
        stage.show();
        
        DataManager manager = (DataManager) data;
        JsonObject object =  loadJSONFile(filePath);
        String name = object.getString("Name");
        manager.setName(name);
        int borderThickness = object.getInt("Border Thickness");
        String borderColor = object.getString("Border Color");
        JsonArray array = object.getJsonArray("Cities, Capitals, and Leaders");
        for(int i=0; i<array.size(); i++){
            JsonObject subregion = array.getJsonObject(i);
            String cityName = subregion.getString("City");
            String capitalName = subregion.getString("Capital");
            String leaderName = subregion.getString("Leader");
            manager.addItem(new Subregion(cityName, capitalName, leaderName));
        }
        progressLabel.setText("25%");
        progressBar.setProgress(.25);
        JsonArray flagLeaderArray = object.getJsonArray("Flag and Leader Photos");
        for(int i=0; i<flagLeaderArray.size(); i++){
            JsonObject subregion = flagLeaderArray.getJsonObject(i);
            String flagphoto = subregion.getString("Flag Photo");
            String leaderphoto = subregion.getString("Leader Photo");
            manager.addFlag(flagphoto);
            manager.addLeader(leaderphoto);
        }
        progressLabel.setText("50%");
        progressBar.setProgress(.5);
        JsonArray colorArray = object.getJsonArray("Colors");
        for(int i=0; i<colorArray.size(); i++){
            JsonObject color = colorArray.getJsonObject(i);
            JsonValue red = color.get("Red");
            manager.addColor(((JsonNumber)red).bigDecimalValue().doubleValue(),0);
            JsonValue green = color.get("Green");
            manager.addColor(((JsonNumber)green).bigDecimalValue().doubleValue(),1);
            JsonValue blue = color.get("Blue");
            manager.addColor(((JsonNumber)blue).bigDecimalValue().doubleValue(),2);
        }
        progressLabel.setText("75%");
        progressBar.setProgress(.75);
        String fileToPolygons = object.getString("Directory");
        progressLabel.setText("100%");
        progressBar.setProgress(1);
        //stage.close();
        
        System.out.println("Name: " + name);
        System.out.println("Border Thickness: " + borderThickness);
        System.out.println("Border Color: " + borderColor);
        manager.printAll();
        System.out.println("Directory to Polygons: " + fileToPolygons);
        
        //more stuff
        String bgColor = object.getString("Background Color");
        String directoryToFlag = object.getString("Directory To Flag");
        int magnification = object.getInt("Magnification");
        int xPosition = object.getInt("X Magnification Position");
        int yPosition = object.getInt("Y Magnification Position");
        System.out.println("Background Color: " + bgColor);
        System.out.println("Directory To Flag: " + directoryToFlag);
        System.out.println("Magnification: " + magnification);
        System.out.println("X Magnification Position: " + xPosition);
        System.out.println("Y Magnification Position: " + yPosition);
        String directoryToImage = object.getString("Directory To Image");
        //manager.reset();
        //manager.reset();
        //manager.updateImage(directoryToImage);
        loadPane(data, fileToPolygons);
        
        //String directoryToImage = object.getString("Directory To Image");
        //manager.
        //Workspace space = (Workspace) app.getWorkspaceComponent();
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) {
        DataManager manager = (DataManager) data;
        boolean hasCapital = true;
        boolean hasFlag = true;
        boolean hasLeader = true;
        JsonArrayBuilder subregionBuilder = Json.createArrayBuilder();
        for(int i=0; i<manager.getItems().size(); i++){
            if(manager.getItems().get(i).getCapital().equals("")){
                hasCapital = false;
            }
            if(manager.getItems().get(i).getLeader().equals("")){
                hasLeader = false;
            }
            JsonObject subregion = Json.createObjectBuilder()
                    .add("name", manager.getItems().get(i).getName())
                    .add("capital", manager.getItems().get(i).getCapital())
                    .add("leader", manager.getItems().get(i).getLeader())
                    .add("red", manager.getColors()[0].get(i)*256)
                    .add("green", manager.getColors()[1].get(i)*256)
                    .add("blue", manager.getColors()[2].get(i)*256).build();
            subregionBuilder.add(subregion);
        }
        JsonArray subregionJSON = subregionBuilder.build();
        //now export holy moly fk this
        JsonObject toExport = Json.createObjectBuilder()
                .add("name", manager.getName())
                .add("subregions_have_capitals", hasCapital)
                .add("subregions_have_flags", hasFlag)
                .add("subregions_have_leaders", hasLeader)
                .add("subregions", subregionJSON).build();
        
        try{
            saveData(data, filePath, toExport);
        }
        catch(Exception e){
            e.printStackTrace();
        }
   }

    @Override
    public void importData(AppDataComponent data, String filePath) {
        System.out.println("asdfasd import");
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    public JsonObject exportIntoJSON(String[][] nameArray,
            File[][] fileArray, Color[] colorArray, String name, int borderThickness,
            String color, String filePath, String backgroundColor, String directoryToFlag,
            int magnification, int xPosition, int yPosition, String directoryToImage) throws IOException{
        /*
        ArrayList<Polygon>[] polygonArray;
        String[][] nameArray;
        File[][] fileArray;
        Color[] colorArray;
        */
        
        JsonArrayBuilder nameBuilder = Json.createArrayBuilder();
        for(int i=0; i<nameArray.length; i++){
            JsonObject item = Json.createObjectBuilder()
                    .add("City",nameArray[i][0])
                        .add("Capital", nameArray[i][1])
                        .add("Leader", nameArray[i][2]).build();
            nameBuilder.add(item);
        }
        JsonArray nameJSON = nameBuilder.build();
        
        JsonArrayBuilder fileBuilder = Json.createArrayBuilder();
        for(int i=0; i<fileArray.length; i++){
            JsonObject item = Json.createObjectBuilder()
                    .add("Flag Photo", fileArray[i][0].toString())
                        .add("Leader Photo", fileArray[i][1].toString()).build();
            fileBuilder.add(item);
        }
        JsonArray fileJSON = fileBuilder.build();
        
        JsonArrayBuilder colorBuilder = Json.createArrayBuilder();
        for(int i=0; i<colorArray.length; i++){
            JsonObject item = Json.createObjectBuilder()
                    .add("Red", colorArray[i].getRed())
                        .add("Green", colorArray[i].getGreen())
                        .add("Blue", colorArray[i].getBlue()).build();
            colorBuilder.add(item);
        }
        JsonArray colorJSON = colorBuilder.build();
        JsonObject toSave = Json.createObjectBuilder()
                .add("Name", name)
                .add("Border Thickness", borderThickness)
                .add("Border Color", color)
                .add("Cities, Capitals, and Leaders", nameJSON)
                .add("Flag and Leader Photos", fileJSON)
                .add("Colors", colorJSON)
                .add("Directory", filePath)
                .add("Background Color", backgroundColor)
                .add("Directory To Flag", directoryToFlag)
                .add("Magnification", magnification)
                .add("X Magnification Position", xPosition)
                .add("Y Magnification Position", yPosition)
                .add("Directory To Image", directoryToImage)
                .build();
        return toSave;
    }
    
    public void loadPane(AppDataComponent data, String filePath) throws IOException{
        //use loadjsonfile method
        // CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
                
        //load number of subregions
        //subregions array:
        //number of polygons in this region
        Polygon poly;
        JsonArray subregionArray = json.getJsonArray(SUBREGIONS);
        JsonArray subregionPolygonArray;
        JsonArray subregionPolygonArrayObjectArray;
        JsonObject subregionPolygonObject;
        JsonObject xAndYObject;
        //polygonGroup = new Group();
        
        try{
            dataManager.changeLengthOfArray(subregionArray.size());
            //dataManager.setBackgroundToBlue();
            //the number of subregions
            for(int i=0; i<subregionArray.size(); i++){
                subregionPolygonObject = subregionArray.getJsonObject(i);
                subregionPolygonArray = subregionPolygonObject.getJsonArray(SUBREGION_POLYGONS);
                //the number of subregion polygons
                for(int j=0; j<subregionPolygonArray.size(); j++){
                    //put in values of array into the arraylist of arrays
                    //create jsonobject?
                    subregionPolygonArrayObjectArray = subregionPolygonArray.getJsonArray(j);
                    poly = new Polygon();
                    poly.setStroke(Color.BLACK);
                    poly.setStrokeWidth(.005);
                    //this iterates over the points in per subregion polygon
                    for(int k=0; k<subregionPolygonArrayObjectArray.size(); k++){
                        xAndYObject = subregionPolygonArrayObjectArray.getJsonObject(k);
                        //add the points to the polygon to evenutally return
                        addPoints(poly, xAndYObject, dataManager.getWidthAndHeight());
                    }
                    dataManager.addListener(poly, i);
                    dataManager.addPolygon(poly, i);
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        dataManager.displayData();
        //dataManager.addPolygonsToGroup(polygonGroup);
    }
    
    
    public void addPoints(Polygon poly, JsonObject object, double[] widthAndHeight){
        poly.getPoints().addAll(new Double[]{
            (getDataAsDouble(object, JSON_X)+180)/360*widthAndHeight[0],
            (180-(getDataAsDouble(object, JSON_Y)+90))/180*widthAndHeight[1]
        });
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
}
